FROM ubuntu:18.04

USER root

WORKDIR /home/ubuntu

RUN apt-get update

RUN apt-get install -y software-properties-common curl git

RUN add-apt-repository ppa:bitcoin/bitcoin; \
    curl -sL https://deb.nodesource.com/setup_8.x | bash -; \
    apt-get update

RUN apt-get install -y build-essential libtool autotools-dev automake pkg-config libssl-dev libevent-dev bsdmainutils \
    python3 libboost-system-dev libboost-filesystem-dev libboost-chrono-dev libboost-test-dev libboost-thread-dev \
    libminiupnpc-dev libzmq3-dev libqt5gui5 libqt5core5a libqt5dbus5 qttools5-dev qttools5-dev-tools libprotobuf-dev \
    protobuf-compiler libqrencode-dev libdb4.8-dev libdb4.8++-dev libboost-program-options-dev libboost1.48-* libboost-all-dev

COPY . /home/ubuntu/syscoin

WORKDIR /home/ubuntu/syscoin

RUN ./autogen.sh
RUN ./configure CXXFLAGS="--param ggc-min-expand=1 --param ggc-min-heapsize=32768"
RUN make
RUN make install

WORKDIR /home/ubuntu